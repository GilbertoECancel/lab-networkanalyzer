#include <vector>
#include<iostream>
#include "packet.h"

/// \file

/// \fn void FilterBySrcAddr(vector<Packet> &netdata, string address)
/// \~English
/// Function that filters the packets in the netflow by the source address
/// \param netdata packets vector
/// \param address key address to filter
/// \~Spanish
/// Funcion que filtra los paquetes en el flujo de red por la direccion fuente
/// \param netdata vector de paquetes
/// \param address direccion llave para el filtro
void FilterBySrcAddr(vector<Packet> &netdata, string address){

     // YOUR CODE HERE

    for(unsigned int contador = 0; contador < netdata.size(); contador++)
    {

        string source = netdata[contador].getSrcAddr();

        if(source != address)
            netdata[contador].disable();

    }
}

/// \fn void FilterByDstAddr(vector<Packet> &netdata, string address)
/// \~English
/// Function that filters the packets in the netflow by the destination address
/// \param netdata packets vector
/// \param address key address to filter
/// \~Spanish
/// Funcion que filtra los paquetes en el flujo de red por la direccion destino
/// \param netdata vector de paquetes
/// \param address direccion llave para el filtro
void FilterByDstAddr(vector<Packet> &netdata, string address){

     // YOUR CODE HERE

    for(unsigned int contador = 0; contador < netdata.size(); contador++)
    {
    string direccion = netdata[contador].getDstAddr();

    if(direccion != address)
        netdata[contador].disable();
    }
}

/// \fn void FilterBySrcPort(vector<Packet> &netdata, int port)
/// \~English
/// Function that filters the packets in the netflow by the source port
/// \param netdata packets vector
/// \param port key port to filter
/// \~Spanish
/// Funcion que filtra los paquetes en el flujo de red por el puerto fuente
/// \param netdata vector de paquetes
/// \param port puerto llave para el fitro
void FilterBySrcPort(vector<Packet> &netdata, int port){

     // YOUR CODE HERE
    for(unsigned int contador = 0; contador < netdata.size(); contador++)
    {
        int source = netdata[contador].getSrcPort();

        if(source != port)
            netdata[contador].disable();

    }

}

/// \fn void FilterByDstPort(vector<Packet> &netdata, int port)
/// \~English
/// Function that filters the packets in the netflow by the destination port
/// \param netdata packets vector
/// \param port key port to filter
/// \~Spanish
/// Funcion que filtra los paquetes en el flujo de red por el puerto destino
/// \param netdata vector de paquetes
/// \param port puerto llave para el fitro
void FilterByDstPort(vector<Packet> &netdata, int port){

     // YOUR CODE HERE

    for(unsigned int contador = 0; contador < netdata.size(); contador++)
    {
        int destino = netdata[contador].getDstPort();

        if(destino != port)
            netdata[contador].disable();

    }

}

