#include "packet.h"
#include <string>
#include <fstream>
#include <vector>

/// \file

using namespace std ;

/// \fn bool ReadFile(string fname, vector<Packet> & netdata)
/// \~English
/// \brief Function that reads a file and return a boolean
/// that determines if the file was succesfully opened and read or not.
/// \param fname  String that contains the filename of the file.
/// \param netdata  Vector of packets.
/// \return true if successfuly read a file, false otherwise
/// \~Spanish
/// \brief Funcion que lee un archivo y devuelve un booleano que determina
/// si el archivo abrio y se leyo con exito o no.
/// \param fname  Cadena de caracteres que contiene el nombre del archivo.
/// \param netdata  Vector de paquetes.
/// \return cierto si leyo el archivo con exito, false de lo contrario
bool ReadFile(string fname, vector<Packet> & netdata){

    ifstream infile;
    string sa ; // temp source address
    string da ; // temp dest address
    int sp ;    // temp src port (puerto fuente)
    int dp ;    // temp dst port (puerto destino)
    int oct ;   // temp octets (octetos)
    int pkt ;   // temp packets (paquetes)

    infile.open(fname.c_str());

    if(!infile.is_open())
       return false ;

    while(infile>> sa >> da >> sp >> dp >> oct >>pkt ){
        Packet p(sa, da, sp, dp, oct, pkt) ;
        netdata.push_back(p) ;
    }

    infile.close() ;
    return true ;
}

/// \fn bool SaveFile(string fname, vector<Packet> netdata)
/// \~English
/// \brief Function that saves a file to the computer.
/// It returns true when the file was succesfully saved and false
/// if an error occurred.
/// \param fname  String that contains the filename of the file.
/// \param netdata  Vector of packets.
/// \return true if successfuly read a file, false otherwise
/// \~Spanish
/// \brief Funcion que guarda un archivo en la computadora.
/// Devuelve cierto cuando el archivo se guardo con exito y falso si
/// ocurre error
/// \param fname  Cadena de caracteres que contiene el nombre del archivo.
/// \param netdata  Vector de paquetes.
/// \return cierto si guardo el archivo con exito, false de lo contrario
bool SaveFile(string fname, vector<Packet> netdata){

    ofstream outfile;

    outfile.open(fname.c_str());

    if(!outfile.is_open())
       return false ;

    for(unsigned int i = 0; i < netdata.size(); i++){
        outfile << netdata.at(i).getSrcAddr() << " " << netdata.at(i).getDstAddr() << " "
                << netdata.at(i).getSrcPort() << " " << netdata.at(i).getDstPort() << " "
                << netdata.at(i).getOctects() << " " << netdata.at(i).getPackets() << endl ;
    }

    outfile.close() ;
    return true ;
}
