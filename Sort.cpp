#include <string>
#include <vector>
#include "packet.h"
#include <iostream>

/// \file

/// \fn void SortBySrcAddr(vector<Packet> &netdata)
/// \~English
/// \brief Function that sorts by source address
/// the packets in the netflow file using the Bubble sort algorithm.
/// \param netdata  Packet vector that will be sorted.
/// \~Spanish
/// \brief Funcion que ordena por direccion fuente
/// los paquetes en el archivo de netflows usando el algoritmo de Bubble sort (Burbuja).
/// \param netdata Vector de paquetes a ser ordenado.
void SortBySrcAddr(vector<Packet> &netdata){

    // Space to implement the Bubble Sort algorithm
    // YOUR CODE HERE

    Packet almazeno;

    for(int x = 0; x < netdata.size(); x++){

        for(int y = 0; y < (netdata.size()) -1; y++){

            if(netdata[y].getSrcAddr() > netdata[y + 1].getSrcAddr()){

                almazeno = netdata[y];

                netdata[y] = netdata[y + 1];

                netdata[y + 1] = almazeno;

            }
        }

    }




}


/// \fn void SortByDstAddr(vector<Packet> &netdata)
/// \~English
/// \brief Function that sorts by destination address
/// the packets in the netflow file using the Selection sort algorithm.
/// \param netdata  Packet vector that will be sorted.
/// \~Spanish
/// \brief Funcion que ordena por direccion destino
/// los paquetes en el archivo de netflows usando el algoritmo de Selection sort (Seleccion).
/// \param netdata Vector de paquetes a ser ordenado.
void SortByDstAddr(vector<Packet> &netdata){

    // Space to implement the Selection Sort algorithm
    // YOUR CODE HERE

    unsigned int minimo;
    Packet almazen;
    for(unsigned int x = 0; x < (netdata.size()) - 1; x++)
    {
        minimo = x;

        for(unsigned int y = x + 1; y < netdata.size(); y++)
        {

            if(netdata[y].getDstAddr() < netdata[minimo].getDstAddr())

                almazen = netdata[y];

                netdata[x] = netdata[minimo];

                netdata[minimo] = almazen;
        }
    }



}

/// \fn void SortBySrcPort(vector<Packet> &netdata)
/// \~English
/// \brief Function that sorts by source port
/// the packets in the netflow file using the Bubble sort algorithm.
/// \param netdata  Packet vector that will be sorted.
/// \~Spanish
/// \brief Funcion que ordena por puerto fuente
/// los paquetes en el archivo de netflows usando el algoritmo de Bubble sort (Burbuja).
/// \param netdata Vector de paquetes a ser ordenado.
void SortBySrcPort(vector<Packet> &netdata){

    // Space to implement the Bubble Sort algorithm
    // YOUR CODE HERE

    Packet almazeno;

    for(int x = 0; x < netdata.size(); x++){

        for(int y = 0; y < (netdata.size()) -1; y++){

            if(netdata[y].getSrcPort() > netdata[y + 1].getSrcPort()){

                almazeno = netdata[y];

                netdata[y] = netdata[y + 1];

                netdata[y + 1] = almazeno;

            }
        }

    }



}

/// \fn void SortByDstPort(vector<Packet> &netdata)
/// \~English
/// \brief Function that sorts by destination port
/// the packets in the netflow file using the Selection sort algorithm.
/// \param netdata  Packet vector that will be sorted.
/// \~Spanish
/// \brief Funcion que ordena por puerto destino
/// los paquetes en el archivo de netflows usando el algoritmo de Selection sort (Seleccion).
/// \param netdata Vector de paquetes a ser ordenado.
void SortByDstPort(vector<Packet> &netdata){

    // Space to implement the Selection sort algorithm
    // YOUR CODE HERE

    int minimo;
    Packet almazen;
    for(unsigned int x = 0; x < (netdata.size()) - 1; x++)
    {
        minimo = x;

        for(unsigned int y = x + 1; y < netdata.size(); y++)
        {

            if(netdata[y].getDstPort() < netdata[minimo].getDstPort())

                almazen = netdata[y];

                netdata[x] = netdata[minimo];

                netdata[minimo] = almazen;
        }
    }



}
